import 'dart:async';
import 'package:accounting/models/user-model.dart';
import 'package:meta/meta.dart';
import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';



class UserRepository {

  Future<UserModel> authenticate({@required String email,@required String password,}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Dio dio = new Dio();
    // await Future.delayed(Duration(seconds: 1));
    print("dsdsdsdsdsdsdsdsddsdsdsdsdsdsdsdsddsdsdsdsdsdsdsdsddsdsdsdsdsdsdsdsd");
    try{
    Map<String, String> headersMap = {
      'Content-Type' : 'application/json',
      'Accept' : 'application/json',
    };
    dio.options.headers = headersMap;
    Response response = await dio.post("http://panel.vgashimov.webfactional.com/api/auth/login", data: {"email": email.trim(), "password": password.trim()});
     print(response.data);
    // print(response.statusCode);

    UserModel userModel = UserModel();
    if(response.statusCode == 200) {
      userModel = UserModel.fromJson(response.data);

      prefs.setString('apiToken', userModel.accessToken.toString());
//        token = response.data['apiToken'];
      // print(response.data.toString()['apiToken']);
    }else{
      print("zad");
//        userModel = null;
    }
    return userModel;
    } on DioError catch(e) {

//      throw ('Email və ya şifrə səhvdir');
      throw ('${e}');
    }
    print("-------------------------------------------");
//      print(prefs.getString('apiToken'));
    print("********************************************");
//      print(token.toString());


  }


  Future<void> deleteToken() async {
    /// delete from keystore/keychain

    SharedPreferences prefs = await SharedPreferences.getInstance();

    Dio dio = new Dio();
    // await Future.delayed(Duration(seconds: 1));
    Map<String, String> headersMap = {
      'Content-Type' : 'application/json',
      'Accept' : 'application/json',
      'Authorization' :'Bearer ' + prefs.getString('apiToken').toString(),
    };
    dio.options.headers = headersMap;
    Response response = await dio.post("http://panel.vgashimov.webfactional.com/api/auth/logout");
    print("bla-bla-bla-bla-bla-bla-bla-bla-bla-bla-bla-bla-bla-bla-");
    if (response.statusCode == 200){
      prefs.setString('apiToken', null);
    }
    print(response.statusCode);
    print(response.data);
    print("!!!!!!!!!!!!!!!!!!!****!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    print("_____________________________-----------------------------------");
    print("bla-bla-bla-bla-bla-bla-bla-bla-bla-bla-bla-bla-bla-bla-");
    print(response.data['apiToken']);
    return;
  }

  Future<void> persistToken(String token) async {
    /// write to keystore/keychain
    Dio dio = new Dio();
    // await Future.delayed(Duration(seconds: 1));
    Map<String, String> headersMap = {
      'Content-Type' : 'application/json',
      'Accept' : 'application/json',
      'Authorization' :'Bearer ' + token,
    };
    dio.options.headers = headersMap;
    Response response = await dio.post("http://panel.vgashimov.webfactional.com/api/auth/me");
    // print(response.statusCode);
    if(response.statusCode == 200) {
      token = response.data['apiToken'];
      // print(response.data.toString()['apiToken']);
    }else{
      print("zad");
    }



    return;
    // return;
  }

  Future<bool> checkToken(String token) async {
    /// write to keystore/keychain
    Dio dio = new Dio();
    Map<String, String> headersMap = {
      'Content-Type' : 'application/json',
      'Accept' : 'application/json',
      'Authorization' : 'Bearer ' + token,
    };
    try{
      dio.options.headers = headersMap;
      Response response = await dio.post("http://panel.vgashimov.webfactional.com/api/auth/me");
      token = response.data['apiToken'];
      return true;
    }catch(exception){
      return false;
    }



  }

  Future<bool> hasToken() async {
    // / read from keystore/keychain
    // /
    print("has token ");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String apiToken = prefs.getString('apiToken');
    if (apiToken != null){
      print("first_if");
      if(await checkToken(apiToken)){
        print('second if');
        return true;
      }
      print(apiToken);
      // return true;
      print("first is end ");
    }

    print("%%^^^^^^^^^^^^^^^^--------------------------^^^^^^");
    return false;
  }




  Future<List<dynamic>> signUp({String name, String email, String number,String type}) async {
    Dio dio = new Dio();
    // await Future.delayed(Duration(seconds: 1));
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String apiToken = prefs.getString('apiToken').toString();
    var dataHub = {
      "token":apiToken,
      "name": name.trim(),
      "email": email.trim(),
      "password": 'password',
//      "password_confirmation":password.trim(),
      "type": type,
    };
    try{
    Response response = await dio.post("http://panel.vgashimov.webfactional.com/api/auth/register", data: dataHub);

    UserModel userModel = UserModel();

    if(response.statusCode == 200) {
      // token = response.data['apiToken'];
      userModel = UserModel.fromJson(response.data);
      print(response.data['apiToken']);
      return [true, 'Əla'];
    }else{
      print("zad");
      return [false,"Xeta"];
    }
    }catch(exception){

      return [false,exception.message];
    }
  }

  Future<bool> isSignedIn() async {
    // / read from keystore/keychain
    // /
    print("has token ");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String apiToken = prefs.getString('apiToken');
    if (apiToken != null){
      print("first_if");
      if(await checkToken(apiToken)){
        print('second if');
        return true;
      }
      print(apiToken);
      // return true;
      print("first is end ");
    }

    print("%%^^^^^^^^^^^^^^^^--------------------------^^^^^^");
    return false;
  }

  Future<User> getUser() async {
    print("lalalaallallallalalalalalalaallallallalalala");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String apiToken = prefs.getString('apiToken').toString();
    Dio dio = new Dio();
    Map<String, String> headersMap = {
      'Content-Type' : 'application/json',
      'Authorization' : 'Bearer ' + apiToken,
    };

    User userModel = User();

    var dataHub = {
      'token' :apiToken
    };
    try{
      dio.options.headers = headersMap;
      Response response = await dio.post("http://panel.vgashimov.webfactional.com/api/auth/me",data: dataHub);
//      if(response.statusCode == 200) {
      print(response.data);
      userModel = User.fromJson(response.data);
//      }

      print(response.data);
      print("-----!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
      print(userModel.id);
      return userModel;
    }catch(exception){

      print("*********************** catch ${exception.toString()} *******************************");
      return userModel;
    }
  }

  Future<String> changeProfile(String name, String oldPassword ,String newPassword ) async {
    print("lalalaallallallal- changeProfile - alalalalalaallallallalalala");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String apiToken = prefs.getString('apiToken').toString();
    Dio dio = new Dio();
    Map<String, String> headersMap = {
      'Content-Type' : 'application/json',
      'Authorization' : 'Bearer ' + apiToken,
    };
    var dataHub = {
      "token":apiToken,
      "full_name": name.trim(),
      "oldPassword": oldPassword.trim(),
      "newPassword": newPassword.trim()
    };

    try{
      dio.options.headers = headersMap;
      Response response = await dio.post("http://panel.vgashimov.webfactional.com/api/auth/changepassword",data: dataHub);
      if(response.statusCode == 200) {

        print("*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*!");
        print(response.data);
        print("*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*!");
//        Map<String, dynamic> json = response.data;
//        print(json);
        print(response.data['token']);
        if(response.data['token'].toString().trim().length>0){
          print(response.data['token']);
          prefs.setString('apiToken', response.data['token']);
        }
        print(response.data);
      }
      print(response.statusCode);
      print("!!!!!!!!!!!!!!!!!!!****!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
//      print(userModel.id);
      return 'token';
    } on DioError catch(exception){

//      print("*********************** catch ${exception} *******************************");
      return 'error';
    }
  }
}