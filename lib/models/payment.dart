
class PaymentData {
  List<Payments> payments;
  String error;

  PaymentData({this.payments});
//
//  PaymentData.fromJson(Map<String, dynamic> json) {
//    if (json['payments'] != null) {
//      payments = new List<Payments>();
//      json['payments'].forEach((v) {
//        payments.add(new Payments.fromJson(v));
//      });
//    }
//  }


  PaymentData.fromJson(Map<String, dynamic> json)
      : payments =
      (json['data']["payments"] as List).map((i) => new Payments.fromJson(i)).toList(),
        error = "";

  PaymentData.withError(String errorValue)
      : payments = List(),
        error = errorValue;



  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.payments != null) {
      data['payments'] = this.payments.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Payments {
  int id;
  String clientName;
  int clientId;
  int workerId;
  int received;
  String date;
  String confirmed;
  List<Amounts> amounts;

  Payments(
      {
        this.id,
        this.clientName,
        this.clientId,
        this.workerId,
        this.received,
        this.date,
        this.confirmed,
        this.amounts
      });

  Payments.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    clientName = json['client_name'];
    clientId = json['client_id'];
    workerId = json['worker_id'];
    received = json['received'];
    confirmed = json['confirmed'];
    date = json['date'];
    if (json['amounts'] != null) {
      amounts = new List<Amounts>();
      json['amounts'].forEach((v) {
        amounts.add(new Amounts.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['client_name'] = this.clientName;
    data['client_id'] = this.clientId;
    data['worker_id'] = this.workerId;
    data['received'] = this.received;
    data['confirmed'] = this.confirmed;
    data['date'] = this.date;
    if (this.amounts != null) {
      data['amounts'] = this.amounts.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Amounts {
  int id;
  int amount;
  String currencyTitle;

  Amounts({this.id, this.amount, this.currencyTitle});

  Amounts.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    amount = json['amount'];
    currencyTitle = json['currency_title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['amount'] = this.amount;
    data['currency_title'] = this.currencyTitle;
    return data;
  }
}