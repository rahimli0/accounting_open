
class CurrencyData {
  List<CurrencyModel> currencies;
  String error;
  

  CurrencyData.fromJson(Map<String, dynamic> json)
      : currencies =
  (json['data']["currencies"] as List).map((i) => new CurrencyModel.fromJson(i)).toList(),
        error = "";

  CurrencyData.withError(String errorValue)
      : currencies = List(),
        error = errorValue;


  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.currencies != null) {
      data['data']['currencies'] = this.currencies.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CurrencyModel {
  int id;
  String title;

  CurrencyModel(
      {
        this.id,
        this.title
      });

  CurrencyModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    return data;
  }
}
