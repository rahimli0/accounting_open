import 'package:accounting/components/datetime_picker.dart';
import 'package:accounting/models/payment.dart';
import 'package:accounting/models/user-model.dart';
import 'package:accounting/pages/common/loading.dart';
import 'package:accounting/pages/home/payments/add/screen.dart';
import 'package:accounting/repositories/user-repostiory.dart';
import 'package:flutter/material.dart';

import 'bloc.dart';
import 'bloc_provider.dart';


class PaymentsPage extends StatefulWidget {
  @override
  _PaymentsPageState createState() => _PaymentsPageState();
}

class _PaymentsPageState extends State<PaymentsPage> {
  DateTime _fromDate = DateTime(
    DateTime.now().year,
    DateTime.now().month,
    DateTime.now().day - 10,
    0,
    0,
    0,
  );
  DateTime _toDate = DateTime(
    DateTime.now().year,
    DateTime.now().month,
    DateTime.now().day,
    23,
    59,
    59,
  );
  bool showFilter = false;

  UserRepository repository = UserRepository();

  PaymentsPageBloc bloc;
  @override
  void didChangeDependencies() {
    bloc = PaymentsPageBlocProvider.of(context);
    bloc.fetchAllPayments(_fromDate,_toDate,0,0);
  }

  @override
  Widget build(BuildContext context) {
    bloc.fetchAllPayments(_fromDate,_toDate,0,0);
    return Scaffold(
      appBar: AppBar(
        title: Text("Ödənişlər"),
        centerTitle: true,
      ),
      floatingActionButton: buildAddUserButton(),
      body:
          Container(
            child:
            Padding(
//              padding: const EdgeInsets.only(bottom: 20,left: 20,right: 20),
              padding: const EdgeInsets.all(0),
              child: Column(
                children: <Widget>[
                  Card(
                      child: ListTile(
                        onTap: (){
                          setState(() {
                            showFilter = !showFilter;
                          });},
                        title: Text("Filtr"),
                        leading: Icon(Icons.filter_list),
                        trailing: showFilter ? Icon(Icons.keyboard_arrow_up):Icon(Icons.keyboard_arrow_down),
                      ),
                    ),
                  Container(
                    padding: const EdgeInsets.only(bottom: 20,left: 20,right: 20),
                    child:
                    showFilter ?
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          child: DateTimePicker(
                            labelText: 'Başlanğıc tarix',
                            selectedDate: _fromDate,
                            selectDate: (DateTime date) {
                              print(date);
                              setState(() {
                                _fromDate = date;
                              });
                            },
                          ),
                        ),
                        SizedBox(width: 24),
                        Expanded(
                          child: DateTimePicker(
                            labelText: 'Son tarix',
                            selectedDate: _toDate,
                            selectDate: (DateTime date) {
                              setState(() {
                                _toDate = DateTime(
                                  date.year,
                                  date.month,
                                  date.day,
                                  23,
                                  59,
                                  59,
                                );
                              });
                            },
                          ),
                        ),
                      ],
                    )
                        :SizedBox(),
                  ),
                  Expanded(
                      child:StreamBuilder<PaymentData>(
                        stream: bloc.subject.stream,
                        builder: (context, AsyncSnapshot<PaymentData> snapshot) {
                          if (snapshot.hasData) {
                            if (snapshot.data.error != null && snapshot.data.error.length > 0){
                              return _buildErrorWidget(snapshot.data.error);
                            }
                            return _buildMainDataWidget(snapshot.data);

                          } else if (snapshot.hasError) {
                            return _buildErrorWidget(snapshot.error);
                          } else {
                            return LoadingPage();
                          }
                        },
                      ),
                  )
                ],
              ),
            ),
          ),
    );
  }

  Widget _buildErrorWidget(Object error) {
    return Center(
      child: Text("Xəta baş verdi, təkrar cəhd edin"),
    );
  }

  Widget _buildMainDataWidget(PaymentData data) {

    return data.payments.length == 0 ? Center(child: Text("Ödəniş yoxdur"),):
    ListView.builder(
      itemCount: data.payments.length,
      itemBuilder: (BuildContext context ,int index){
        Payments payment_item = data.payments[index];
        return Card(
          child: ListTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: payment_item.amounts.map((item) => Text("${item.amount} ${item.currencyTitle}")).toList(),
            ),
            leading: Icon(payment_item.received == 1 ?Icons.arrow_downward:Icons.arrow_upward,color: payment_item.received == 1 ? Colors.green : Colors.red,),
            trailing: Text("${payment_item.clientName}"),
            subtitle: Text('${payment_item.date.toString()}'),
          ),
        );
      },
    );
  }


  Widget buildAddUserButton() {
    return FutureBuilder<User>(
        future: repository.getUser(),
        builder: (BuildContext context, AsyncSnapshot<User> snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data!=null) {
              if(snapshot.data.type == 'worker') {
                return FloatingActionButton(
                  child: Icon(Icons.add),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => PaymentCreatePage()),
                    );
                  },
                );
              }else{
                return Visibility(
                  visible: false,
                  child: FloatingActionButton(
                    onPressed: (){},
                  ),
                );
              }
            } else {
              return Visibility(
                visible: false,
                child: FloatingActionButton(
                  onPressed: (){},
                ),
              );
            }
          }else{
            return Visibility(
              visible: false,
              child: FloatingActionButton(
                onPressed: (){},
              ),
            );}
        }
    );
  }


}
