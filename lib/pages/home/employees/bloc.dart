import 'package:accounting/models/user-model.dart';
import 'package:accounting/repository/user-repository.dart';
import 'package:rxdart/rxdart.dart';

class EmployeesPageBloc {

  final Repository _repository = Repository();
  final BehaviorSubject<WorkerData> _subject =
  BehaviorSubject<WorkerData>();

//  Observable<WorkerData> get subject => _subject.stream;

  fetchAllEmployees() async {
    WorkerData employeeData = await _repository.fetchAllWorkers();
    print("TEstTEstTEstTEstTEstTEstTEstTEstTEstTEstTEstTEst");
    print(employeeData.toString());
    _subject.sink.add(employeeData);
  }



  BehaviorSubject<WorkerData> get subject => _subject;

  dispose() {
    _subject.close();
  }
}

//final categories_bloc = EmployeesPageBloc();
