import 'package:flutter/material.dart';

import 'bloc.dart';



class PaymentDetailPageBlocProvider extends InheritedWidget {
  final PaymentDetailPageBloc bloc;

  PaymentDetailPageBlocProvider({Key key, Widget child})
      : bloc = PaymentDetailPageBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static PaymentDetailPageBloc of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(PaymentDetailPageBlocProvider)
    as PaymentDetailPageBlocProvider)
        .bloc;
  }
}
