
import 'package:accounting/models/user-model.dart';
import 'package:accounting/pages/authentication/authentication.dart';
import 'package:accounting/pages/common/loading.dart';
import 'package:accounting/pages/home/settings/detail/screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc.dart';
import 'bloc_provider.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  SettingsPageBloc bloc;
  @override
  void didChangeDependencies() {
    bloc = SettingsPageBlocProvider.of(context);
    bloc.fetchMeData();
  }
  bool is_show = false;
  @override
  Widget build(BuildContext context) {
    final AuthenticationBloc authenticationBloc =
    BlocProvider.of<AuthenticationBloc>(context);
    return
      Scaffold(
        appBar:  AppBar(
          title: Text("Ayarlar",style: TextStyle(inherit: true,color: Colors.black),),
          centerTitle: true,
        ),
        body: StreamBuilder<User>(
        stream: bloc.subject.stream,
          builder: (context, AsyncSnapshot<User> snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data.id == null){
                return _buildErrorWidget(snapshot.data);
              }
              is_show = true;
              return _buildMainDataWidget(snapshot.data,authenticationBloc);

            } else if (snapshot.hasError) {
              return _buildErrorWidget(snapshot.error);
            } else {
              print("_________________ Loadin 1 defe ------------------------------");
              return LoadingPage();
            }
          },
        ),
      );
  }

  Widget _buildErrorWidget(error) {
    return Container(
//      height: MediaQuery.of(context).size.height / 1.4,
      child: Center(
        child: Text("Xəta baş verdi, təkrar cəhd edin"),
      ),
    );
  }

  Widget _buildMainDataWidget(User user_item,AuthenticationBloc authenticationBloc) {
    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!^^^^^");
    print(user_item.name);
    return
      Column(
        children: <Widget>[
          Card(
            child: ListTile(
              title: Text("${user_item.fullName !=null ?user_item.fullName:''}",),
              subtitle: Text('${user_item.email}'),
              trailing: Icon(Icons.keyboard_arrow_right),
              leading: Icon(Icons.person),
              onTap: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ProfileChangePage(username: user_item.fullName,)),
                );

              },
            ),
          ),
          Card(
            child: ListTile(

              title: Text("Çıxış",style: TextStyle(color: Colors.red),),
              leading: Icon(Icons.exit_to_app,color: Colors.red),
              onTap: (){
                authenticationBloc.dispatch(LoggedOut());
              },
            ),
          )
        ],
      );
  }
}

