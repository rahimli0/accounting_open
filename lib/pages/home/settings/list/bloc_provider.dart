import 'package:accounting/pages/home/settings/list/bloc.dart';
import 'package:flutter/material.dart';



class SettingsPageBlocProvider extends InheritedWidget {
  final SettingsPageBloc bloc;

  SettingsPageBlocProvider({Key key, Widget child})
      : bloc = SettingsPageBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static SettingsPageBloc of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(SettingsPageBlocProvider)
    as SettingsPageBlocProvider)
        .bloc;
  }
}
