import 'package:accounting/models/user-model.dart';
import 'package:accounting/repositories/user-repostiory.dart';
import 'package:accounting/repository/user-repository.dart';
import 'package:rxdart/rxdart.dart';

class SettingsPageBloc {

  final UserRepository _repository = UserRepository();
  final BehaviorSubject<User> _subject =
  BehaviorSubject<User>();

//  Observable<ClientData> get subject => _subject.stream;

  fetchMeData() async {
    User meData = await _repository.getUser();
    print("TEstTEstTEstTEstTEstTEstTEstTEstTEstTEstTEstTEst");
    print(meData.toString());
    _subject.sink.add(meData);
  }



  BehaviorSubject<User> get subject => _subject;

  dispose() {
    _subject.close();
  }
}

//final categories_bloc = SettingsPageBloc();
