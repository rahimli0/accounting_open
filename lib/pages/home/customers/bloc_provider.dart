import 'package:accounting/pages/home/customers/bloc.dart';
import 'package:flutter/material.dart';



class CustomersPageBlocProvider extends InheritedWidget {
  final CustomersPageBloc bloc;

  CustomersPageBlocProvider({Key key, Widget child})
      : bloc = CustomersPageBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static CustomersPageBloc of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(CustomersPageBlocProvider)
    as CustomersPageBlocProvider)
        .bloc;
  }
}
