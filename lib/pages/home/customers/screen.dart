
import 'package:accounting/models/user-model.dart';
import 'package:accounting/pages/common/loading.dart';
import 'package:accounting/pages/home/customers/add/screen.dart';
import 'package:accounting/pages/home/customers/bloc.dart';
import 'package:accounting/pages/home/customers/bloc_provider.dart';
import 'package:flutter/material.dart';

import 'detail/bloc_provider.dart';
import 'detail/detail.dart';

class CustomersPage extends StatefulWidget {
  @override
  _CustomersPageState createState() => _CustomersPageState();
}

class _CustomersPageState extends State<CustomersPage> {
  CustomersPageBloc bloc;
  @override
  void didChangeDependencies() {
    bloc = CustomersPageBlocProvider.of(context);
    bloc.fetchAllCustomers();
  }


  static paymentDetailPage(int customer_id,String customer_name,BuildContext context) {
    final page = PaymentDetailPageBlocProvider(
      child: PaymentDetailPage(
        customer_id: customer_id,
        customer_name: customer_name,
      ),
    );
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) {
        return page;
      }),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:  AppBar(
//        iconTheme: IconThemeData(color: Colors.black),
//        brightness: Brightness.light,
//        backgroundColor: Colors.white,
//        elevation: 0.3,
        title: Text("Müştərilər",style: TextStyle(inherit: true,color: Colors.black),),
        centerTitle: true,
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        backgroundColor: Colors.cyan,
        onPressed: (){

          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => CustomerAddPage()),
          );
        },
      ),
      body: StreamBuilder<ClientData>(
        stream: bloc.subject.stream,
        builder: (context, AsyncSnapshot<ClientData> snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.error != null && snapshot.data.error.length > 0){
              return _buildErrorWidget(snapshot.data.error);
            }
            return _buildMainDataWidget(snapshot.data);

          } else if (snapshot.hasError) {
            return _buildErrorWidget(snapshot.error);
          } else {
            return LoadingPage();
          }
        },
      ),
    );
  }

  Widget _buildErrorWidget(String error) {
    return Center(
      child: Text("Xəta baş verdi, təkrar cəhd edin"),
    );
  }




  Widget _buildMainDataWidget(ClientData data) {
    return data.users.length == 0 ? Center(child: Text("Müştəri yoxdur"),):ListView.builder(
        itemCount: data.users.length,
        itemBuilder: (BuildContext context,int index){
          User user_item = data.users[index];
          return Card(
              child: ListTile(
                title: Text("${user_item.fullName}",),
                subtitle: Text('${user_item.email}'),
                trailing: Icon(Icons.keyboard_arrow_right),
                leading: Icon(Icons.person),
                onTap: (){
                  print("sldlskdsldlskdsldlskdsldlskdsldlskd");
                  paymentDetailPage(user_item.id,user_item.fullName,context);
                },
              ),
            );
        },
      );
  }
}

