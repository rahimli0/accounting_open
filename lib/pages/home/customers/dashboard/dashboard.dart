import 'dart:convert';

import 'package:accounting/models/income-outcome.dart';
import 'package:accounting/models/user-model.dart';
import 'package:accounting/pages/common/loading.dart';
import 'package:accounting/pages/home/customers/bloc_provider.dart';
import 'package:accounting/pages/home/customers/screen.dart';
import 'package:accounting/pages/home/dashboard/bloc.dart';
import 'package:accounting/pages/home/dashboard/bloc_provider.dart';
import 'package:accounting/pages/home/widgets/credit_card_widget.dart';
import 'package:accounting/repositories/provider.dart';
import 'package:accounting/repositories/user-repostiory.dart';
import 'package:accounting/repository/user-repository.dart';
import 'package:flutter/material.dart';
import 'package:accounting/components/datetime_picker.dart';


class CustomerDashboardPage extends StatefulWidget {
  @override
  _CustomerDashboardPageState createState() => _CustomerDashboardPageState();
}

class _CustomerDashboardPageState extends State<CustomerDashboardPage> {
  DateTime _fromDate = DateTime(

    DateTime.now().year,
    DateTime.now().month,
    DateTime.now().day - 10,
    0,
    0,
    0,
  );
  DateTime _toDate = DateTime(
    DateTime.now().year,
    DateTime.now().month,
    DateTime.now().day,
    23,
    59,
    59,
  );
  User _selectedClient;
  List<User> _clients = [];
  bool _isLoading = true;
  DataProvider dataProvider = DataProvider();
  @override
  void initState() {
    super.initState();
    _isLoading = true;
    _getUser();

  }




  User currentUser ;
  bool user_loading = true;
  UserRepository userRepository = UserRepository();
  _getUser() async {

    UserRepository repository = UserRepository();
    User client_result = await repository.getUser();
    if(client_result.id != '' && client_result.id != null){
      setState(() {
        currentUser = client_result;
        _selectedClient = client_result;
        user_loading = false;
      });
    }else{

    }

  }


//  CustomerDashboardPageBloc bloc;
//  @override
//  void didChangeDependencies() {
//    bloc = CustomerDashboardPageBlocProvider.of(context);
//    bloc.fetchAllCustomers();
//  }
  Widget projectWidget() {

    List<Widget> list = new List<Widget>();
    print("^^&&&&&&^^****&^&&^&*&*&*&^&*&^&^&^*&^&^*&^&^*&^&^*^*^&^**&^^");
    print("^^&&&&&&^^****&^&&^&*&*&*&^&*&^&^&^*&^&^*&^&^*&^&^*^*^&^**&^^");
    return FutureBuilder(
      builder: (context, projectSnap) {
        if (projectSnap.connectionState == ConnectionState.none &&
            projectSnap.hasData == null) {
          //print('project snapshot data is: ${projectSnap.data}');
          return Container(
            child: Text("sdsd"),
          );
        }

        print("^%^&*^&*^*(^&*^^&*&^^%^&*^&*^*(^&*^^&*&^^%^&*^&*^*(^&*^^&*&^");
        print(projectSnap.data);
        print("^%^&*^&*^*(^&*^^&*&^^%^&*^&*^*(^&*^^&*&^^%^&*^&*^*(^&*^^&*&^");
          if(projectSnap.data != null) {
            _isLoading = false;
            IncomeOutcomeData incomeOutcomeData = projectSnap.data;
            return Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 12.0, horizontal: 20.0),
                  child: CreditCardWidget(title: "Qəbul edilən məbləğ",
                      value_azn: "10 Azn",
                      value_usd: "0 Usd",
                      value: incomeOutcomeData.income,
                      themeColor: Colors.cyan),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 12.0, horizontal: 20.0),
                  child: CreditCardWidget(title: "Ödənilən məbləğ",
                      value_azn: "0 Azn",
                      value_usd: "0 Usd",
                      value: incomeOutcomeData.outcome,
                      themeColor: Colors.red),
                )
              ],
            );
          }else{
            if(projectSnap.hasError){
              return Center(
                child: Text("Xəta baş verdi, təkrar cəhd edin"),
              );
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          }
      },
      future: dataProvider.fetchDashboardData(_fromDate, _toDate, _selectedClient != null?_selectedClient.id:0),
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        elevation: 0.3,
        title: Text("İzahat",style: TextStyle(inherit: true,color: Colors.black),),
        centerTitle: true,
      ),
      body:
      ListView(
        children: <Widget>[
          Container(
            height: 120,
            child:
            Padding(
              padding: const EdgeInsets.only(bottom: 20,left: 20,right: 20),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(
                        child: DateTimePicker(
                          labelText: 'Başlanğıc tarix',
                          selectedDate: _fromDate,
                          selectDate: (DateTime date) {
                            print(date);
                            setState(() {
                              _fromDate = date;
                            });
                          },
                        ),
                      ),
                      SizedBox(width: 24),
                      Expanded(
                        child: DateTimePicker(
                          labelText: 'Son tarix',
                          selectedDate: _toDate,
                          selectDate: (DateTime date) {
                            setState(() {
                              _toDate = DateTime(
                                date.year,
                                date.month,
                                date.day,
                                23,
                                59,
                                59,
                              );
                            });
                          },
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
//          StreamBuilder
          user_loading?Container(child: Center(child: CircularProgressIndicator(),),):
          projectWidget(),
        ],
        shrinkWrap: true,
        physics: BouncingScrollPhysics(),
      ),
    );
  }
}

