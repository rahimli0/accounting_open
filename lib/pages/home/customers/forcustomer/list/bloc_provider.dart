import 'package:flutter/material.dart';

import 'bloc.dart';



class PaymentForUserPageBlocProvider extends InheritedWidget {
  final PaymentForUserPageBloc bloc;

  PaymentForUserPageBlocProvider({Key key, Widget child})
      : bloc = PaymentForUserPageBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static PaymentForUserPageBloc of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(PaymentForUserPageBlocProvider)
    as PaymentForUserPageBlocProvider)
        .bloc;
  }
}
