import 'package:accounting/models/payment.dart';
import 'package:accounting/models/user-model.dart';
import 'package:accounting/repository/user-repository.dart';
import 'package:rxdart/rxdart.dart';

class PaymentForUserPageBloc {

  final Repository _repository = Repository();
  final BehaviorSubject<PaymentData> _subject =
  BehaviorSubject<PaymentData>();

//  Observable<PaymentData> get subject => _subject.stream;

  fetchAllPayments(DateTime from_date,DateTime to_date,int worker_id,int client_id,String cw) async {
    PaymentData employeeData = await _repository.fetchPaymentsDetailList(from_date, to_date, worker_id, client_id,cw);
    print("TEstTEstTEstTEstTEstTEstTEstTEstTEstTEstTEstTEst");
    print(employeeData.toString());
    _subject.sink.add(employeeData);
  }



  BehaviorSubject<PaymentData> get subject => _subject;

  dispose() {
    _subject.close();
  }
}

//final categories_bloc = PaymentForUserPageBloc();
